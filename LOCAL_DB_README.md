# Setting up Local Database with Docker Compose

Follow these instructions to set up a local database for development purposes using Docker Compose.

## Prerequisites

Make sure you have the following installed on your local machine:

- Docker
- Docker Compose

To verify, run the following commands in your terminal:

```docker -v```

```docker-compose -v```

## Preparation

For data security reasons, the database credentials are not stored in the repository. You need to copy the `.env.example` file to `.env`. This file is ignored by git and will not be committed to the repository. You can do this by running the following command:

```cp .env.example .env```

The folder structure should look like this:

![Copy .env.example to .env](https://i.imgur.com/zcEX8jL.png)

## Starting the Database

The database and the dump file is located in the `Database Backup` directory. To start the database, run the following command:

```cd Database\ Backup```

```sh db_start.sh```

Alternatively, you can run the following command:

```docker-compose up -d```

## Stopping the Database

To stop the database, run the following command:

```sh db_stop.sh```

Alternatively, you can run the following command:

```docker-compose stop```

## Resetting/Rebuilding the Database

To reset the database, run the following command:

```sh db_reset.sh```

This requires the database to be running first. Alternatively, you can run the following command:

```docker-compose down -v```

## Updating the Database

For security reasons, the database credentials are not stored in the repository. So, the database is not synced automatically. If you need to update the database, please export a new dump file and replace the existing one in the `Database Backup` directory. Then perform a reset as mentioned above.