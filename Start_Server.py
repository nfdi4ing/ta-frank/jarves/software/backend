
# Author:   Tobias Hamann
# Title:    Test_Setup.py
# Date:     29.03.2022
# Version:  0.3.1
# Description: This file is supposed to start up the server.

NormalStart=False

if NormalStart==True:
    # Option 1: Use this. 
    
    import os

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings") 
    
    from django.core.management import call_command
    
    # This can be complemented by things as
    # from django.core.wsgi import get_wsgi_application 
    # application = get_wsgi_application()

    call_command('runserver')

    # --> Klappt nicht!
else:
    # Option 2: Use only if desperate.
    import os
    os.system('python D:/Jarves/backend/DjangoAPI/manage.py runserver')
    # Can be stopped with Crtl+C (instead of CTRL-BREAK as stated in terminal)
