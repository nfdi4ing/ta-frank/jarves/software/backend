
# Author:   Tobias Hamann
# Title:    Test_Setup.py
# Date:     29.03.2022
# Version:  0.4.0
# Description: This file is supposed to check your python version, your environment as well as your Django installation.

# Note: This file is far from perfect/best practice. It is intended as a temporary file in the early development stage.

# Check python Version
import sys
print(" ")
print("Python version is:")
print (sys.version)
print("Python version should be 3.9.7")
print(" ")
print("Version info.")
print (sys.version_info)

# Check environment, in which python is running
import sys

def get_base_prefix_compat():
    """Get base/real prefix, or sys.prefix if there is none."""
    return getattr(sys, "base_prefix", None) or getattr(sys, "real_prefix", None) or sys.prefix

def in_virtualenv():
    return get_base_prefix_compat() != sys.prefix

# The following is not very nicely implemented, but should work for checking the environment
print(" ")
print("--> Checking environment...")
print("Current Prefix is:")
print (sys.prefix)
print(" ")
print("Base Prefix is:")
print (sys.base_prefix)
print(" ")


print ("This might be the correct environment. ")
print (sys.prefix)
print ("should look like ~\ backend\ .venv")
print ("(Without the spaces after the \)")



# Check Django
print(" ")
print("Django Version is:")
from django import get_version
DjangoVersion=get_version()
print(DjangoVersion)
print("Django Version should be 4.0.3")