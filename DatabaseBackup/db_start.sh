#!/bin/bash

# Check if Docker is installed
if ! [ -x "$(command -v docker)" ]; then
    echo "Error: Docker is not installed. Please install Docker before running this script."
    exit 1
fi
# Build Docker image
docker-compose up -d