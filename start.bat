@echo off

::: Starting
:::          __                          
:::         / /___ _______   _____  _____
:::    __  / / __ `/ ___/ | / / _ \/ ___/
:::   / /_/ / /_/ / /   | |/ /  __(__  ) 
:::   \____/\__,_/_/    |___/\___/____/  
:::                                      

for /f "delims=: tokens=*" %%A in ('findstr /b ::: "%~f0"') do @echo(%%A

%CD%\.venv\Scripts\activate  && cd DjangoAPI && python manage.py runserver

echo c:\GitLab-CE\\backend\.venv\Scripts\activate.bat&& cd DjangoAPI && python manage.py runserver