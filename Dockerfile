FROM python:3.9

WORKDIR /app

COPY requirements.txt .

#RUN - apt -y install apt-utils && apt -y update && apt -y install python3.9 python3-pip && apt -y upgrade && pip3 install -r requirements.txt && pip install django-extensions

RUN pip install -r requirements.txt

COPY . .

EXPOSE 8000

ARG PASSWORD
ENV PASSWORD=$PASSWORD

ARG SECRET_KEY
ENV SECRET_KEY=$SECRET_KEY
ENV DJANGO_ENVIRONMENT=production

CMD ["python", "DjangoAPI/manage.py", "makemigrations"]
CMD ["python", "DjangoAPI/manage.py", "migrate"]
CMD ["python", "DjangoAPI/manage.py", "runserver", "0.0.0.0:8000"]
