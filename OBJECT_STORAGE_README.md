# Setting up Local Database with Docker Compose

Follow these instructions to set up a object storage for development purposes using Docker Compose.

## Prerequisites

Make sure you have the following installed on your local machine:

- Docker
- Docker Compose

To verify, run the following commands in your terminal:

```docker -v```

```docker-compose -v```

## Preparation

Please update your `.env` file from the `.env.example` file. It has the default credentials for the object storage.

## Starting the Object Storage

cd into the `DatabaseBackup` directory (It uses the same directory for local database and object storage) and run the following command:

```cd DatabaseBackup```

```sh db_start.sh```

Alternatively, you can run the following command:

```docker-compose up -d```

## Viewing the Object Storage

You can view the object storage by visiting the following URL:

```http://localhost:9001```

You need to login with the credentials from the `.env` file.

## Dummy Example

You can find an implementation of the object storage in the `DjangoAPI/Policies/views.py` file at the end. This uploads a file to the object storage and retrieves it.