import logging


class JarvesRouter:
    """
    A router to control all your database operations on models in the
    standard Django Jarves_db.sqlite3 Database.
    """

    logger = logging.getLogger("Jarves_db_logger")

    # Define Apps that shall be routed by this router here:
    route_app_labels = {'auth', 'contenttypes'} #'ProjectAPP',

    # Debug:
    logger.debug("Das ist der Jarves_db_Datenbanktest")

    def db_for_read(self, model, **hints):
        """
        Attempts to read auth and contenttypes models go to 'Jarves_db'.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'Jarves_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth and contenttypes models go to 'Jarves_db'.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'Jarves_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth or contenttypes apps is
        involved.
        """
        if (
            obj1._meta.app_label in self.route_app_labels or
            obj2._meta.app_label in self.route_app_labels
        ):
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth and contenttypes apps only appear in the
        'Jarves_db' database.
        """
        print (app_label)
        if app_label in self.route_app_labels:
            return db == 'Jarves_db'
        return None
