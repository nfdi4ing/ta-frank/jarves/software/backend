from apiservices.models import UserProfile
from rest_framework import renderers
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.generics import GenericAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action
from django.core.paginator import Paginator

from django.views import View
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.contenttypes.models import ContentType

from .models import Project, ProjectArtifacts, ProjectFiles, ProjectFindings, ProjectPhase, ProjectStep, UserTextCommunicationBox
from .serializers import ProjectArtifactSerializer, ProjectFileSerializer, ProjectFindingSerializer, ProjectSerializer, ProjectPhaseSerializer, ProjectStepSerializer, UserTextCommunicationSerializer

import logging

from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from rest_framework import status
from django.http import JsonResponse
from rest_framework.decorators import action

from rest_framework import serializers
from rest_framework import viewsets

from rest_framework import generics
from .models import UserTextCommunicationBox
from .serializers import UserTextCommunicationSerializer

from Policies.models import Colleg as University, Funding, Institute, ProjectPolicy, ProjectChecklistTask

from itertools import chain
from django_minio_backend import MinioBackend

# Initialize logger
logger = logging.getLogger(__name__)


# Create your views here.
# See also: https://stackoverflow.com/questions/40324756/where-can-i-access-request-parameters-in-django-rest-framework
# For Filter options see: https://docs.djangoproject.com/en/4.0/topics/db/queries/#field-lookups

class ProjectView (ListCreateAPIView):

    # Specify the authentication classes
    authentication_classes = [SessionAuthentication]
    
    # Specify the permission classes
    permission_classes = [IsAuthenticated]
    # Queryset of all the objects availiable in the view.
    # Custom querysets can be set with get_queryset(). See https://www.django-rest-framework.org/api-guide/generic-views/#get_querysetself
    queryset = Project.objects.all()
    #queryset = queryset.filter(users__user=self.request.user.id)

    # Select Serializer to serialize, validate, deserialize input and output
    serializer_class = ProjectSerializer

    logger.info("ProjectView has been executed!")

    


class SingleProjectView (RetrieveUpdateDestroyAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

class SinglePhaseView (RetrieveUpdateDestroyAPIView):
    queryset = ProjectPhase.objects.all()
    serializer_class = ProjectPhaseSerializer

class SingleStepView (RetrieveUpdateDestroyAPIView):
    queryset = ProjectStep.objects.all()
    serializer_class = ProjectStepSerializer

class ProjectViewSet(ModelViewSet):

    # Specify the authentication classes
    authentication_classes = [SessionAuthentication]
    
    # Specify the permission classes
    permission_classes = [IsAuthenticated]

    allowed_methods = ['GET', 'POST']
    
    # Queryset of all the objects availiable in the view.
    # queryset = Project.objects.all()
    # projectcount = queryset.count()
    projectcount = None  # TODO: is this ever read before writing?

    # Select Serializer to serialize, validate, deserialize input and output
    serializer_class = ProjectSerializer

    # Logging actions
    logger.debug("ProjectViewSet has been executed...")
    logger.debug(authentication_classes)

    def get_queryset(self)->Project:
        """
        Optionally restricts the returned projects to a given title,
        by filtering against a `title` query parameter in the URL.
        """
        logger.debug(self.request.user.id)
        # Get the search criteria for the queryset
        #title = self.request.query_params.get('title', None)
        attribute = self.request.query_params.get('attribute', None)
        filter = self.request.query_params.get('filter', None)
        page_number = self.request.query_params.get('pageNumber', None)
        page_size = self.request.query_params.get('pageSize', None) 
        sorted_by = self.request.query_params.get('sortedBy', None)
        sort_order = self.request.query_params.get('sortOrder', None)

          
        # Get the queryset:
        # the order_by () only works while creating the queryset, so the workaround is to first check what order is wanted and then create the queryset, 
        # watch out for the hardcoded conditions of sort_order (asc, desc) but it works...
        if sorted_by is None:
            queryset = Project.objects.all().order_by('id')

        if sorted_by is not None:
            if sort_order == 'asc' or sort_order is None :
                 queryset = Project.objects.all().order_by(sorted_by)

            if sort_order == 'desc' :
                 queryset = Project.objects.all().order_by('-' + sorted_by)

        '''
        # If there is search criteria for the queryset, filter queryset by it, otherwise leave returned data untouched.
        if title is not None:
            queryset = queryset.filter(title__icontains = title)
            # __icontains means, that the title param may be included in the results, while the i of icontains means it is NOT case sensitive.
        '''

        if filter is not None:
            if attribute is not None:
                if not hasattr(Project,attribute): # Django sql injection, risk?
                    raise ValueError(f'given {attribute=} is not applicable')
                
                kwargs = {f'{attribute}__icontains': filter}
                # sets kwargs dynamicly, queryset.filter(f'{attribute}__icontains'= filter), puts the string in attribute in the **__icontains command and checks for filter
                queryset = queryset.filter(**kwargs)
                # __icontains means, that the title param may be included in the results, while the i of icontains means it is NOT case sensitive.
                self.projectcount = queryset.count()
        else:
            self.projectcount = queryset.count()

        #print(queryset)
        #print(self.request.user.id)
        #queryset = queryset.filter(users__id__contains = self.request.user.id)
        #project_users = Project.objects.filter(users__in=person.specialties.all())
        queryset = queryset.filter(users__user=self.request.user.id)

        if page_size is not None:
            # paginator, the paginator sequences the items in pages of item amount 'page_size'
            # it only does so if the page_size is not None
            paginator = Paginator(queryset, per_page = page_size)

            # Must be raised by one because first page in angular is 0 and django first page is 1 and so on
            page_number= 1 + int(page_number)
            
            #if page_number == 0:
            #    page_number = 1
            page_obj = paginator.get_page(page_number)
            
            # If the page isn’t a number, it returns the first page. If the page number is negative or greater than the number of pages, it returns the last page (https://docs.djangoproject.com/en/4.1/ref/paginator/#django.core.paginator.Paginator.page)
            # apparently also applies for page_number = 0

            return page_obj


        return queryset
    

    def finalize_response(self, request, response, *args, **kwargs):
        response = super().finalize_response(request, response, *args, **kwargs)
        response['ProjectCount'] = self.projectcount
        response['Access-Control-Expose-Headers'] = 'ProjectCount'
        return response
        

    @action(detail=False, methods=['post'])
    def update_project_user(self, request, pk):
        project = Project.objects.filter(id=pk).first()
        user_id = int(request.data.get('user'))

        # Get or create user profile based on user_id
        user_profile, created = UserProfile.objects.get_or_create(user_id=user_id)

        # Convert the queryset to a list
        current_project_userlist = list(project.users.all())

        # Append the new user profile to the list
        current_project_userlist.append(user_profile)

        try:
            # Set the updated user list for the project
            project.users.set(current_project_userlist)
            project.save()
            return JsonResponse({'success': 'User added'})
        except Project.DoesNotExist:
            print("Project not found")
            return JsonResponse({'error': 'User cannot be added to the project'}, status=400)

    @action(detail=False, methods=['post'])
    def add_policy(self, request, pk):
        project = Project.objects.filter(id=pk).first()
        if not project:
            return JsonResponse({'error': 'Project not found'}, status=400)
        
        logger.info("Adding Policies to ProjectID: "+ str(project.id))

        policies_to_add = []

        uni_id = int(request.data.get('university'))
        institute_id = int(request.data.get('institute'))
        funding_id = int(request.data.get('funding'))

        university = University.objects.filter(id=uni_id).first()
        institute = Institute.objects.filter(id=institute_id).first()
        funding = Funding.objects.filter(id=funding_id).first()
        
        uni_policy = university.policy if university and university.policy else None
        inst_policy = institute.policy if institute and institute.policy else None
        funding_policy = funding.policy if funding and funding.policy else None

        if funding_policy:
            policies_to_add.append(funding_policy)
        if inst_policy:
            policies_to_add.append(inst_policy)
        if uni_policy:
            policies_to_add.append(uni_policy)

        #logger.info("policies to add:")
        #logger.info(policies_to_add)
            
        for policy in policies_to_add:

            #logger.info("policy:")
            #logger.info(policy)
            ProjectPolicy.objects.get_or_create(
                project=project,
                name=policy.name,
                kind=policy.kind,
                dmp_recommendation=policy.dmp_recommendation,
                storage_recommendation=policy.storage_recommendation,
                score1=policy.score1,
                score2=policy.score2                
            )        
        return JsonResponse({'success': 'Policy added'})
    
    # Add the correct Tasks from Checklists to the project
    @action(detail=False, methods=['post'])
    def add_checklist(self, request, pk):

        # Find out the project to add the tasks to
        project = Project.objects.filter(id=pk).first()
        if not project:
            return JsonResponse({'error': 'Project not found'}, status=400)
        
        logger.info("Adding tasks to ProjectID: "+ str(project.id))
        
        tasks_to_add = []

        # Get the checklists by:

        # Getting the IDs of the Uni, Institute and Funding Org from the request
        uni_id = int(request.data.get('university'))
        institute_id = int(request.data.get('institute'))
        funding_id = int(request.data.get('funding'))
        
        # Translating them into their corresponding objects from their corresponding table
        university = University.objects.filter(id=uni_id).first()
        institute = Institute.objects.filter(id=institute_id).first()
        funding = Funding.objects.filter(id=funding_id).first()
        
        # Getting their policy object
        uni_policy = university.policy if university and university.policy else None
        inst_policy = institute.policy if institute and institute.policy else None
        funding_policy = funding.policy if funding and funding.policy else None
        
        # Getting their tasks by the policy object
        uni_tasks = uni_policy.tasks.all() if university and uni_policy.tasks.first() else None
        inst_tasks = inst_policy.tasks.all() if institute and inst_policy.tasks.first() else None
        funding_tasks = funding_policy.tasks.all() if funding and funding_policy.tasks.first() else None
        
        # Append the tasks to the list of tasks to be added
        if funding_tasks:
            tasks_to_add.append(funding_tasks)
        if inst_tasks:
            tasks_to_add.append(inst_tasks)
        if uni_tasks:
            tasks_to_add.append(uni_tasks)

        # Get only unique tasks form tasks_to_add list
        tasks_to_add = list(chain.from_iterable(tasks_to_add))

        logger.info("Tasks to add:")
        logger.info(tasks_to_add)
        
        # Copy all Tasks to the Project--------
        for task in tasks_to_add:
            ProjectChecklistTask.objects.get_or_create(
                project=project,
                name=task.name,
                description_en=task.description_en,
                description_de=task.description_de,
                phase=task.phase,
                step=task.step,
                tag = task.tag                
            )        
        return JsonResponse({'success': 'Tasks added'})

class ProjectHighlight(GenericAPIView):
    # Specify the authentication classes
    authentication_classes = [SessionAuthentication]
    
    # Specify the permission classes
    permission_classes = [IsAuthenticated]

    def get_queryset(self)->Project:
        queryset = Project.objects.all()
        queryset = queryset.filter(users__user=self.request.user.id)
    renderer_classes = [renderers.StaticHTMLRenderer]

    # Logging actions
    logger.debug("ProjectHighlight has been executed")

    def get(self, request, *args, **kwargs):
        project = self.get_object()
        return Response(project.description)

        #response = UserTextCommunication.objects.all()
        project_id = request.query_params.get('project_id')
        response = UserTextCommunication.objects.filter(project=project_id).all()
        return JsonResponse({'text_fields': response})


class UserTextCommunicationDetailView(generics.RetrieveUpdateAPIView):
    # Specify the authentication classes
    authentication_classes = [SessionAuthentication]
    
    # Specify the permission classes
    permission_classes = [IsAuthenticated]

    queryset = UserTextCommunicationBox.objects.all()
    serializer_class = UserTextCommunicationSerializer

    def get_object(self):
        # Retrieve the UserTextCommunication object for the specific project
        project_id = self.kwargs.get('project_id')
        obj, created = UserTextCommunicationBox.objects.get_or_create(project_id=project_id)
        return obj
    
    def post(self, request, *args, **kwargs):
        project_id = self.kwargs.get('project_id')
        user_text_communication, created = UserTextCommunicationBox.objects.get_or_create(project_id=project_id)

        # Serialize the incoming data
        serializer = UserTextCommunicationSerializer(user_text_communication, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProjectFindingsView(ModelViewSet):
    queryset = ProjectFindings.objects.all()
    serializer_class = ProjectFindingSerializer
    
    # def get_object(self):
    #     return super().get_object()

    def create(self, request, *args, **kwargs):
        project_id = self.kwargs.get('project_id')
        project = Project.objects.filter(id=project_id).first()
        finding = ProjectFindings.objects.create(project=project, **request.data)
        serializer = ProjectFindingSerializer(finding)
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProjectArtifactsView(ModelViewSet):
    queryset = ProjectArtifacts.objects.all()
    serializer_class = ProjectArtifactSerializer

    # def retrieve(self, request, pk=None):
    #     return super().retrieve(request, pk)

    def create(self, request, *args, **kwargs):
        finding_id = self.kwargs.get('finding_id')
        finding = ProjectFindings.objects.filter(id=finding_id).first()
        artifact = ProjectArtifacts.objects.create(finding=finding, **request.data)
        serializer = ProjectArtifactSerializer(artifact)
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    @action(detail=False, methods=['post'])
    def upload_file(self, request):
        artifact_id = request.data.get('artifact_id') if request.data.get('artifact_id') else 1
        upload_file = request.FILES['file']
        file_name = upload_file.name
        file_path = 'execution/artifacts/' + str(artifact_id) + '/generate/' + file_name
        status = MinioBackend(bucket_name='django-backend-dev-private')._save(file_path, upload_file)
        
        if status:
            content_type = ContentType.objects.get_for_model(ProjectArtifacts)
            file_object = ProjectFiles.objects.create(file_name=file_name, file_path=file_path, content_type=content_type, object_id=artifact_id)
            return JsonResponse({'success': 'File uploaded successfully'})
        return JsonResponse({'error': 'File upload failed'}, status=400)

    @action(detail=True, methods=['get'])
    def get_file_list(self, request, pk=None):
        content_type = ContentType.objects.get_for_model(ProjectArtifacts)
        files = ProjectFiles.objects.filter(content_type=content_type, object_id=pk)
        return JsonResponse({'files': ProjectFileSerializer(files, many=True).data})
    
    @action(detail=False, methods=['get'])
    def download_file(self, request):
        file_id = request.query_params.get('file_id')
        project_file = ProjectFiles.objects.filter(id=file_id).first()
        if not project_file:
            return JsonResponse({'error': 'File not found'}, status=400)
        file_path = project_file.file_path
        file_name = project_file.file_name
        project_file = MinioBackend(bucket_name='django-backend-dev-private')._open(file_path)
        response = HttpResponse(project_file, content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename=' + file_name
        return response