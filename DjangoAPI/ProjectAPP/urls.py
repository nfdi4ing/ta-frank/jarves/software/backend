from django.db import router
from django.urls import path, include
from . import views
# from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter

from . import views
from .views import SingleProjectView, SinglePhaseView, SingleStepView, UserTextCommunicationDetailView, ProjectView, ProjectViewSet

router = DefaultRouter()


# ViewSets are registered with a router class
router.register(r'ProjectViewSet', views.ProjectViewSet, basename='ProjectViewSet')
#router.register(r'UserTextCommunicationViewSet', views.UserTextCommunicationViewSet, basename='UserTextCommunicationViewSet')
router.register(r'ProjectFindingViewset', views.ProjectFindingsView, basename='ProjectFindingViewset')
router.register(r'ProjectArtifactViewset', views.ProjectArtifactsView, basename='ProjectArtifactViewset')

urlpatterns = [
    path('', include(router.urls)), 
    path('ProjectView/<int:pk>', SingleProjectView.as_view(), name='project-detail'),
    path('ProjectPhaseView/<int:pk>', SinglePhaseView.as_view(), name='projectphase-detail'),
    path('ProjectStepView/<int:pk>', SingleStepView.as_view(), name='projectstep-detail'),
    path('ProjectViewSet/<int:pk>/adduser', views.ProjectViewSet.as_view({'post': 'update_project_user'}), name='project-add-user'),
    path('user_text_communication/<int:project_id>/', UserTextCommunicationDetailView.as_view(), name='user_text_communication-detail'),
    path('ProjectViewSet/<int:pk>/addpolicy', views.ProjectViewSet.as_view({'post': 'add_policy'}), name='project-add-policy'),
    path('ProjectViewSet/<int:pk>/addchecklist', views.ProjectViewSet.as_view({'post': 'add_checklist'}), name='project-add-checklist')
   ]