# Generated by Django 4.0.3 on 2024-05-23 09:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('ProjectAPP', '0011_remove_project_timespan'),
    ]

    operations = [
        migrations.CreateModel(
            name='UploadFile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_name', models.CharField(max_length=100)),
                ('file_path', models.TextField()),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.contenttype')),
            ],
        ),
        migrations.CreateModel(
            name='ProjectFindings',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('description', models.TextField()),
                ('planning_text', models.TextField()),
                ('project', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ProjectAPP.project')),
            ],
        ),
        migrations.CreateModel(
            name='ProjectArtifacts',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('description', models.TextField()),
                ('planning_text', models.TextField()),
                ('deletable', models.BooleanField(default=False)),
                ('has_sensitive_data', models.BooleanField(default=False)),
                ('is_answering_research_question', models.BooleanField(default=False)),
                ('finding', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ProjectAPP.projectfindings')),
            ],
        ),
    ]
