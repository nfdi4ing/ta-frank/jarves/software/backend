from rest_framework import serializers

from .models import Project, ProjectArtifacts, ProjectFiles, ProjectFindings, UserTextCommunicationBox, ProjectPhase, ProjectStep
from apiservices.models import UserProfile

class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    # url= serializers.HyperlinkedIdentityField(view_name='ProjectViewSet')

    class Meta:
        model = Project
        fields = ['url', 'id', 'title', 'short_title', 'status', 'start_date', 'description', 'duration', 'research_field']

class UserTextCommunicationSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = UserTextCommunicationBox
        fields = ['text_field_2_1','text_field_2_2','text_field_2_3','text_field_2_4','text_field_2_5','text_field_2_6','text_field_2_7','text_field_2_8','text_field_2_9',
        'text_field_3_1','text_field_3_2','text_field_3_3','text_field_3_4','text_field_3_5'
        ,'text_field_3_6','text_field_3_7',
        'text_field_4_1','text_field_4_2','text_field_4_3',
        'text_field_5_1','text_field_5_2'#,'text_field_5_3','text_field_5_4','text_field_5_5'
        ]



class ProjectPhaseSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = ProjectPhase
        fields = ['url', 'id', 'name', 'description' , 'GUI_name' , 'CODE_name']


class ProjectStepSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = ProjectStep
        fields = ['url', 'id', 'name', 'description' , 'GUI_name' , 'CODE_name']

class ProjectFileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProjectFiles
        fields = ['id', 'file_name', 'file_path']

class ProjectFindingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProjectFindings
        fields = ['id', 'name', 'description', 'project', 'planning_text']
        
        
class ProjectArtifactSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProjectArtifacts
        fields = ['id', 'name', 'description', 'finding', 'planning_text', 'deletable', 'has_sensitive_data', 'is_answering_research_question']