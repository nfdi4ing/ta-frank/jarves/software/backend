from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from datetime import timedelta

# Create your models here.

# to use the ManyToManyField the called class must come before the executing class

# 1 versioncm and 1 researchworkflow relate to 1 data repository
# 1..n checklist store 1 datarepository
class DataRepository(models.Model):
    def __str__(self) -> str:
        return str(self.name)
    name = models.CharField(max_length=30)
    faculty_dpmt = models.CharField(max_length=30)
    link = models.URLField()

    # trying to solve the default problem with foreignkey when datarepo is opened, this function shall asign the attribute when called with foreignkey a default value with placeholder parameters
    # should be abled to change them, but as for fising the gitlab pipeline it worked, found this solution in a stackoverflow answer: https://stackoverflow.com/questions/9311996/setting-default-value-for-foreign-key-attribute
    @classmethod
    def get_default_pk(cls):
        datarepo, created = cls.objects.get_or_create(
            name='default datarepo', 
            defaults=dict(faculty_dpmt='this is a placeholder'),
        )
        return datarepo.pk


# 1..n instances of employees and projects relate to one versioncm
class VersionControlManagment(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default= 'NONE')
    modifier = models.CharField(max_length=30)
    # ToDo: add realfunctionality,
    modified = models.IntegerField() 
    rights = models.CharField(max_length=30)
    data_repository = models.ForeignKey(to=DataRepository, on_delete=models.CASCADE, default=DataRepository.get_default_pk)


# single char field
class AllCollaborationFields(models.Model):
    def __str__(self) -> str:
        return self.role
    # not sure if on_delete=models.CASCADE is correct in this instance
    punkt2_1 = models.CharField(max_length=30)
    #punkt3_2 = model

# employee is authorized by am
class AuthorisationManagement(models.Model):
    def __str__(self) -> str:
        return self.role
    # not sure if on_delete=models.CASCADE is correct in this instance
    role = models.CharField(max_length=30)
    rights = models.TextField()
    # ToDo: should be its own class
    project_id = models.CharField(max_length= 30)


# project funded by 1 o 
class Organisation(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30) 
    # max_length can be higher just set a fixed value
    # ToDo: Maybe ROR ID should be its own class
    # ToDo: add realfunctionality, '0000' to be able to run the pipeline
    ror_id = models.IntegerField()


# 1..n metadatas follow mds
class MetadataStandarts(models.Model):
    def __str__(self) -> str:
        return self.name_of_standart
    name_of_standart = models.CharField(max_length=30)
    # ToDo: no attributes defined


# project relates to metadata
class Metadata(models.Model):
    def __str__(self) -> str:
        return self.project_name + ' ' + self.dataset_title
    # in the UML Diaramm it is shown as implemented here, but that doesnt seem to be very efficent
    # ToDo: metadata of a model can be includet through an inner class of metadata in the respective model
    project_name = models.CharField(max_length=30)
    dataset_title = models.CharField(max_length=30)
    project_description = models.TextField()
    contact_info = models.TextField()
    dataset_handle_URL = models.URLField()
    dataset_handle_DOI = models.CharField(max_length=255)
    keywords = models.TextField()
    sponsor = models.CharField(max_length= 30)
    metadatastandarts = models.ManyToManyField(MetadataStandarts)


# 1..n instances of are published by projects
class Publications(models.Model):
    def __str__(self) -> str:
        return self.title
    title = models.TextField()
    authors = models.TextField()
    year = models.DateField()
    publisher = models.TextField()
    link = models.URLField()


# 1..n projectphases contain 1 tm
class TrainingMaterials(models.Model):
    def __str__(self) -> str:
        return self.name
    project_phase= models.CharField(max_length=30, default='NONE')
    name = models.CharField(max_length=30)
    link = models.URLField()


# project consists of 1..n projectphases
class ProjectPhase(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default='NONE')
    description = models.TextField(default='NONE')
    GUI_name = models.CharField(max_length=30, default='NONE')
    CODE_name = models.CharField(max_length=30, default='NONE')
    # ToDo: add realfunctionality
    # milestones = models.IntegerField(default=0)
    # ToDo: item: attribute, not further specified what attribute 



# projectphases consists of 1..n projectsteps
class ProjectStep(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default='NONE')
    GUI_name = models.CharField(max_length=30, default='NONE')
    CODE_name = models.CharField(max_length=30, default='NONE')
    description = models.TextField()



# 0..n projects has requirements
# consists of checklist
class ProjectRequirements(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30)
    deadline = models.DateField()


# 1..n researchwf relate to 1 checklist
class Checklists(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default='NONE')
    project_phase = models.CharField(max_length=30, default='NONE')
    activity = models.CharField(max_length=30)
    project_requirements = models.ForeignKey('ProjectRequirements', on_delete=models.CASCADE, default=None)
    data_repository = models.ForeignKey(to=DataRepository, on_delete=models.CASCADE, default=DataRepository.get_default_pk)


# project is decribed through rwf
# 0..n Tools implement 1..n rwf
# datalife cycle is part of rwf but an extra class
class ResearchWorkflow(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default='NONE')
    project_phase = models.CharField(max_length=30)
    phase_description = models.TextField()
    data_repository = models.ForeignKey(to=DataRepository, on_delete=models.CASCADE, default=DataRepository.get_default_pk)
    checklists = models.ManyToManyField(Checklists)

from apiservices.models import UserProfile
# 1..n Timespan relate to 1 project
# 1..n ontologies contain 1 project
class Project(models.Model):
    # The following allows for Titles to appear in the Admin view of Django
    # Also, to show what is possible, the status of the Project has been added after the project title
    def __str__(self)->str:
        return str(self.title) + ' (' + str(self.status) + ')'
    # Fields/Attributes of the Model/Table "Project"
    title = models.CharField(max_length=30)
    short_title = models.CharField(max_length=30)
    description = models.TextField()
    status = models.CharField(max_length=30)
    start_date = models.DateField()
    # Error message when tried to commit, to really put in a duration must be with ~ datetime.timedelta(...)
    duration = models.DurationField(default=timedelta(seconds=0))
    research_field = models.CharField(max_length=30, default='NONE')
    version_control_managment = models.ManyToManyField(VersionControlManagment)
    organisation = models.ManyToManyField(Organisation)
    metadata = models.OneToOneField(Metadata, on_delete=models.CASCADE, null=True)
    publications = models.ManyToManyField(Publications)
    project_phase = models.ManyToManyField(ProjectPhase)
    project_requirements = models.ManyToManyField(ProjectRequirements)
    research_workflow = models.ForeignKey('ResearchWorkflow', on_delete=models.CASCADE, null=True)
    users = models.ManyToManyField(UserProfile)
    # timespan = models.CharField(max_length=30, default='')


#class TimeSpan(models.Model):
#    def __str__(self) -> str:
#        return self.project + ' ' + self.start_date + ' ' + self.end_date
#    start_date = models.DateField()
#    end_date = models.DateField()
    # ToDo: field: type, reocurring attribute, 
    #project = models.ForeignKey('Project', on_delete=models.CASCADE, default=None)


class Employee(models.Model):
    def __str__(self) -> str:
        return self.first_name + ' ' + self.last_name
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30, default='NONE')
    coscine_token = models.CharField(max_length=150, default='NONE')
#    institute = models.ManyToManyField(Institute)
    version_control_managment = models.ManyToManyField(VersionControlManagment)
    authorisation_management = models.ManyToManyField(AuthorisationManagement)

class Role(models.Model):
    def __str__(self) -> str:
        return self.role_name
    role_name = models.CharField(max_length=30)
    may_read = models.BooleanField(default=False)


class RoleAssign(models.Model):
    def __str__(self) -> str:
        return self.role + ' ' + self.project + ' ' + self.employee
    employee = models.ForeignKey('Employee', on_delete=models.CASCADE)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    role = models.ForeignKey('Role', on_delete=models.CASCADE)
    assign_date = models.DateField()
    status = models.CharField(max_length=30)

# has 1..n instances of projects and can acces projectdata
class Ontologies(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30)
    # ToDo: database: Name
    link = models.URLField()
    project = models.ForeignKey('Project', on_delete=models.CASCADE, default=None)


# 1..n tools have 1 license
class License(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30)
    description = models.TextField()
    link = models.URLField()


class Tools(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30)
    research_work_flow = models.ManyToManyField(ResearchWorkflow)
    license = models.ForeignKey('License', on_delete=models.CASCADE, default=None)


class ReqByUniversity(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default= 'NONE')
    # toDo: University class
    requirment = models.ForeignKey('ProjectRequirements', on_delete=models.CASCADE, )
    founding_source = models.CharField(max_length=30)
    #field: type


class ReqByInstitute(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default= 'NONE')
    #institute = models.ForeignKey('Institute', on_delete=models.CASCADE, null= True)
    requirment = models.ForeignKey('ProjectRequirements', on_delete=models.CASCADE)
    founding_source = models.CharField(max_length=30)
    #field: type


class ReqByOrganisation(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30,default= 'NONE')
    organisation = models.ForeignKey('Organisation', on_delete=models.CASCADE, null= True)
    requirment = models.ForeignKey('ProjectRequirements', on_delete=models.CASCADE)
    founding_source = models.CharField(max_length=30)
    #field: type

class ReqByPartners(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default='NONE')
    # ToDo: Partner class
    requirment = models.ForeignKey('ProjectRequirements', on_delete=models.CASCADE)
    founding_source = models.CharField(max_length=30)
    #field: type


class UserTextCommunicationBox(models.Model):
    def __str__(self):
        return f"UserTextCommunication for Project: {self.project}"
    project = models.OneToOneField(Project, on_delete=models.CASCADE, null=True)

    text_field_2_1 = models.CharField(max_length=1000, default='Fill in some information')
    text_field_2_2 = models.CharField(max_length=1000, default='None')
    text_field_2_3 = models.CharField(max_length=1000, default='None')
    text_field_2_4 = models.CharField(max_length=1000, default='None')
    text_field_2_5 = models.CharField(max_length=1000, default='None')
    text_field_2_6 = models.CharField(max_length=1000, default='None')
    text_field_2_7 = models.CharField(max_length=1000, default='None')
    text_field_2_8 = models.CharField(max_length=1000, default='None')
    text_field_2_9 = models.CharField(max_length=1000, default='None')

    text_field_3_1 = models.CharField(max_length=1000, default='None')
    text_field_3_2 = models.CharField(max_length=1000, default='None')
    text_field_3_3 = models.CharField(max_length=1000, default='None')
    text_field_3_4 = models.CharField(max_length=1000, default='None')
    text_field_3_5 = models.CharField(max_length=1000, default='None')
    
    text_field_3_6 = models.CharField(max_length=1000, default='None')
    text_field_3_7 = models.CharField(max_length=1000, default='None')

    text_field_4_1 = models.CharField(max_length=1000, default='None')
    text_field_4_2 = models.CharField(max_length=1000, default='None')
    text_field_4_3 = models.CharField(max_length=1000, default='None')

    text_field_5_1 = models.CharField(max_length=1000, default='None')
    text_field_5_2 = models.CharField(max_length=1000, default='None')
    #text_field_5_3 = models.CharField(max_length=1000, default='None')
    #text_field_5_4 = models.CharField(max_length=1000, default='None')
    #text_field_5_5 = models.CharField(max_length=1000, default='None')


# This uploadfile class can be utilized to keep track of the files uploaded by the user in relation to various other classes.
# The content_type and object_id fields are used to create a generic relationship with other classes.
# The content_object field is used to access the related object.
# The file itself is stored in MinIO.
class ProjectFiles(models.Model):
    file_name = models.CharField(max_length=100)
    file_path = models.TextField()
    
    # Generic Foreign Key
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    
    def __str__(self):
        return self.file_name

class ProjectFindings(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30)
    description = models.TextField()
    project = models.ForeignKey('Project', on_delete=models.CASCADE, default=None)
    planning_text = models.TextField()
class ProjectArtifacts(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30)
    description = models.TextField()
    finding = models.ForeignKey('ProjectFindings', on_delete=models.CASCADE, default=None)
    planning_text = models.TextField()
    deletable = models.BooleanField(default=False)
    has_sensitive_data = models.BooleanField(default=False)
    is_answering_research_question = models.BooleanField(default=False)