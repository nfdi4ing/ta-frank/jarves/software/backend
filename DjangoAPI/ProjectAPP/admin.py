from django.contrib import admin

# Register your models here.

from .models import Project, Employee, ProjectArtifacts, ProjectFindings, Role, RoleAssign, ProjectPhase, ProjectStep, Metadata, MetadataStandarts, TrainingMaterials, Organisation, AuthorisationManagement, Ontologies, Publications, VersionControlManagment, DataRepository, ResearchWorkflow, Tools, License, Checklists, ProjectRequirements, ReqByUniversity, ReqByInstitute, ReqByOrganisation, ReqByPartners

admin.site.register(Project)
admin.site.register(Employee)
admin.site.register(Role)
admin.site.register(RoleAssign)
admin.site.register(ProjectPhase)
admin.site.register(ProjectStep)
admin.site.register(Metadata)
admin.site.register(MetadataStandarts)
admin.site.register(TrainingMaterials)
admin.site.register(Organisation)
admin.site.register(AuthorisationManagement)
admin.site.register(Ontologies)
admin.site.register(Publications)
admin.site.register(VersionControlManagment)
admin.site.register(DataRepository)
admin.site.register(ResearchWorkflow)
admin.site.register(Tools)
admin.site.register(License)
admin.site.register(Checklists)
admin.site.register(ProjectRequirements)
admin.site.register(ReqByUniversity)
admin.site.register(ReqByInstitute)
admin.site.register(ReqByOrganisation)
admin.site.register(ReqByPartners)
admin.site.register(ProjectFindings)
admin.site.register(ProjectArtifacts)