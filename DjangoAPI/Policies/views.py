from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.db.models import Q
from django.core.paginator import Paginator
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from .models import DMP_Tool, Storage_Tool, Funding, Policyguide, Institute, ProjectPolicy, Policy_kind, ChecklistTask
from .models import Colleg as University
from .serializers import (
    DMP_ToolSerializer, Storage_ToolSerializer, FundingSerializer,
    PolicySerializer, UniversitySerializer, InstituteSerializer,
    ProjectPolicySerializer, Policy_kindSerializer, ChecklistTaskSerializer
)

from ProjectAPP.models import ProjectPhase, ProjectStep

import logging

# Initialize logger
logger = logging.getLogger(__name__)

#dmp

@api_view(['GET'])
def dmp_tool_list(request):
    """
    Retrieve a list of all DMP tools.
    """
    dmp_tools = DMP_Tool.objects.all()
    serializer = DMP_ToolSerializer(dmp_tools, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def dmp_tool_detail(request, pk):
    """
    Retrieve a specific DMP tool by its primary key.
    """
    dmp_tool = get_object_or_404(DMP_Tool, pk=pk)
    serializer = DMP_ToolSerializer(dmp_tool)
    return Response(serializer.data)

@api_view(['POST'])
def create_dmp_tool(request):
    """
    Create a new DMP tool.
    """
    serializer = DMP_ToolSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

#storage

@api_view(['GET'])
def storage_tool_list(request):
    """
    Retrieve a list of all Storage tools.
    """
    storage_tools = Storage_Tool.objects.all()
    serializer = Storage_ToolSerializer(storage_tools, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def storage_tool_detail(request, pk):
    """
    Retrieve a specific Storage tool by its primary key.
    """
    storage_tool = get_object_or_404(Storage_Tool, pk=pk)
    serializer = Storage_ToolSerializer(storage_tool)
    return Response(serializer.data)

@api_view(['POST'])
def create_storage_tool(request):
    """
    Create a new Storage tool.
    """
    serializer = Storage_ToolSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

#funding

@api_view(['GET'])
def funding_list(request):
    """
    Retrieve a list of all Fundings.
    """
    fundings = Funding.objects.all()
    serializer = FundingSerializer(fundings, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def funding_detail(request, pk):
    """
    Retrieve a specific Funding by its primary key.
    """
    funding = get_object_or_404(Funding, pk=pk)
    serializer = FundingSerializer(funding)
    return Response(serializer.data)

@api_view(['POST'])
def create_funding(request):
    """
    Create a new Funding.
    """
    serializer = FundingSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

#policy

@api_view(['GET'])
def policy_list(request):
    """
    Retrieve a list of all Policies.
    """
    policies = Policyguide.objects.all()
    serializer = PolicySerializer(policies, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def policy_detail(request, pk):
    """
    Retrieve a specific Policy by its primary key.
    """
    policy = get_object_or_404(Policyguide, pk=pk)
    serializer = PolicySerializer(policy)
    return Response(serializer.data)

@api_view(['POST'])
def create_policy(request):
    """
    Create a new Policy.
    """
    serializer = PolicySerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

#university

@api_view(['GET'])
def university_list(request):
    """
    Retrieve a list of all Universities.
    """
    universities = University.objects.all()
    serializer = UniversitySerializer(universities, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def university_detail(request, pk):
    """
    Retrieve a specific University by its primary key.
    """
    university = get_object_or_404(University, pk=pk)
    serializer = UniversitySerializer(university)
    return Response(serializer.data)

@api_view(['POST'])
def create_university(request):
    """
    Create a new University.
    """
    serializer = UniversitySerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

# institute

@api_view(['GET'])
def institute_list(request):
    """
    Retrieve a list of all Institutes.
    """
    institutes = Institute.objects.all()
    serializer = InstituteSerializer(institutes, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def institute_detail(request, pk):
    """
    Retrieve a specific Institute by its primary key.
    """
    institute = get_object_or_404(Institute, pk=pk)
    serializer = InstituteSerializer(institute)
    return Response(serializer.data)

@api_view(['POST'])
def create_institute(request):
    """
    Create a new Institute.
    """
    serializer = InstituteSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

# policy

@api_view(['GET'])
def projectpolicy_list(request, pid):
    """
    Retrieve a list of all ProjectPolicies by project id.
    """
    projectpolicies = ProjectPolicy.objects.select_related().filter(project_id=pid)
    serializer = ProjectPolicySerializer(projectpolicies, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def projectpolicy_detail(request, pk):
    """
    Retrieve a specific ProjectPolicy by its primary key.
    """
    projectpolicy = get_object_or_404(ProjectPolicy, pk=pk)
    serializer = ProjectPolicySerializer(projectpolicy)
    return Response(serializer.data)

@api_view(['POST'])
def create_projectpolicy(request):
    """
    Create a new ProjectPolicy.
    """
    serializer = ProjectPolicySerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

# poliy kind 

@api_view(['GET'])
def policy_kind_list(request):
    """
    Retrieve a list of all PolicyKinds.
    """
    policy_kinds = Policy_kind.objects.all()
    serializer = Policy_kindSerializer(policy_kinds, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def policy_kind_detail(request, pk):
    """
    Retrieve a specific PolicyKind by its primary key.
    """
    policy_kind = get_object_or_404(Policy_kind, pk=pk)
    serializer = Policy_kindSerializer(policy_kind)
    return Response(serializer.data)

@api_view(['POST'])
def create_policy_kind(request):
    """
    Create a new PolicyKind.
    """
    serializer = Policy_kindSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

# checklist_task
@api_view(['POST'])
def create_checklist_task(request):
    """
    Create a new checklist_task.
    """
    serializer = ChecklistTaskSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

# project_checklist
@api_view(['POST'])
def create_project_checklist(request):
    """
    Create a new project_checklist.
    """
    serializer = ProjectChecklistSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)

class SingleChecklistTaskView (RetrieveUpdateDestroyAPIView):
    queryset = ChecklistTask.objects.all()
    serializer_class = ChecklistTaskSerializer

class ChecklistTaskViewSet(ModelViewSet):

    # Queryset of all the objects availiable in the view.
    queryset = ChecklistTask.objects.all()
    ChecklistTaskCount = queryset.count()

    # Select Serializer to serialize, validate, deserialize input and output
    serializer_class = ChecklistTaskSerializer

    # Logging actions
    logger.debug("ChecklistTaskViewSet executes")

    
    def get_queryset(self)->ChecklistTask:
        """
        Optionally restricts the returned ChecklistTasks to given params
        """


        # Extract Values from Request
        self.extract_params_from_request()

        # Get all
        queryset = ChecklistTask.objects.all()

        # Get correct queryset
        queryset = self.filter_queryset(queryset)

        # Sort Queryset
        # TODO: Sorting goes here
        
        # Paginate Querryset
        queryset = self.paginate_response(queryset)


        # TODO: Write Paginator and sorting Function and import.

        

        return queryset
    
    def filter_queryset(self,queryset):
        # Here the logic for the returned query is inserted

        # TODO: LOGIC GOES HERE

        # Filtering of querysets:
        # https://www.w3schools.com/django/django_queryset_filter.php 

        # Filter for Phase

        if self.phase is not None or self.step is not None:

            # Get All_Phase and All_Step
            All_Phase = ProjectPhase.objects.get(CODE_name = 'All')
            All_Step = ProjectStep.objects.get(CODE_name = 'All')
            
            # Get ID of the requested Phase from the Phases 
            try:
                Phase = ProjectPhase.objects.get(CODE_name = self.phase).id
            except: 
                Phase = None

            # Get ID of the requested Step from the Steps 
            try:
                Step = ProjectStep.objects.get(CODE_name = self.step).id
            except:
                Step = None

            # Get the queryset
            queryset = ChecklistTask.objects.filter( 
                Q(phase = Phase) | Q(phase = All_Phase),
                Q(step = Step) | Q(step = All_Step)
                ).distinct()
        
        # Get count of results in queryset
        self.ChecklistTaskCount = queryset.count()
            

        # Filter for Step



        # Filter for Language
        #if self.language is not None:
            #queryset = queryset.objects.filter(language = self.language)

        return queryset
            

    def paginate_response(self,queryset):
        if self.page_size is not None:
            # paginator, the paginator sequences the items in pages of item amount 'page_size'
            # it only does so if the page_size is not None
            paginator = Paginator(queryset, per_page = self.page_size)

            # Must be raised by one because first page in angular is 0 and django first page is 1 and so on

            if self.page_number is not None:
                page_number= 1 + int(self.page_number)
            else:
                page_number=1
            
            #if page_number == 0:
            #    page_number = 1
            page_obj = paginator.get_page(page_number)
            
            # If the page isn’t a number, it returns the first page. If the page number is negative or greater than the number of pages, it returns the last page (https://docs.djangoproject.com/en/4.1/ref/paginator/#django.core.paginator.Paginator.page)
            # apparently also applies for page_number = 0

            return page_obj
        return queryset
    
    def extract_params_from_request (self):
        # Extract Values from Request
        self.title = self.request.query_params.get('title', None)
        self.project = self.request.query_params.get('project', None)
        self.fit = self.request.query_params.get('fit', None)
        self.sortOrder = self.request.query_params.get('sortOrder', None)
        self.sortAttribute = self.request.query_params.get('sortAttribute', None)
        self.pageIndex = self.request.query_params.get('pageIndex', None)
        self.pageSize = self.request.query_params.get('pageSize', None)
        self.language = self.request.query_params.get('language', None)
        self.phase = self.request.query_params.get('phase', None)
        self.step = self.request.query_params.get('step', None)
        self.tag = self.request.query_params.get('tag', None)
        self.author = self.request.query_params.get('author', None)
        self.format = self.request.query_params.get('format', None)
        self.comment = self.request.query_params.get('comment', None)
        self.rating = self.request.query_params.get('rating', None)
        self.attribute = self.request.query_params.get('attribute', None)
        self.filter = self.request.query_params.get('filter', None)
        self.page_number = self.request.query_params.get('pageNumber', None)
        self.page_size = self.request.query_params.get('pageSize', None) 
        self.sorted_by = self.request.query_params.get('sortedBy', None)
        self.sort_order = self.request.query_params.get('sortOrder', None)
      
    def finalize_response(self, request, response, *args, **kwargs):
        response = super().finalize_response(request, response, *args, **kwargs)

        # Write ChecklistTaskCountinto header
        response['ChecklistTaskCount'] = self.ChecklistTaskCount

        # Enables 'ChecklistTaskCount' to be read from header
        response['Access-Control-Expose-Headers'] = 'ChecklistTaskCount'
        return response
    

from django_minio_backend import MinioBackend
import tempfile
from django.http import FileResponse
def dummy_file_upload(request):
    # MinioBackend()._save('dummy.txt', request.FILES['file'])
    status = MinioBackend().is_minio_available()
    print(status)
    fp = tempfile.TemporaryFile()
    fp.write(b'Hello world!')
    fp.seek(0)
    status = MinioBackend(bucket_name='django-backend-dev-private')._save('dummy.txt', fp)
    print(status)
    return JsonResponse({'status': 'success'}, status=200)

def dummy_file_download(request):
    fp = MinioBackend(bucket_name='django-backend-dev-private')._open('dummy.txt')
    return FileResponse(fp, as_attachment=True, filename='dummy.txt')
