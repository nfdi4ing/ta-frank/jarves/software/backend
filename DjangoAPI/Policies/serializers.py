from rest_framework import serializers
from .models import DMP_Tool, Storage_Tool, Funding, Policyguide, Institute, ProjectPolicy, Policy_kind, ChecklistTask, ProjectChecklistTask
from .models import Colleg as University

class DMP_ToolSerializer(serializers.ModelSerializer):
    class Meta:
        model = DMP_Tool
        fields = '__all__'

class Storage_ToolSerializer(serializers.ModelSerializer):
    class Meta:
        model = Storage_Tool
        fields = '__all__'

class FundingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Funding
        fields = '__all__'

class PolicySerializer(serializers.ModelSerializer):
    class Meta:
        model = Policyguide
        fields = '__all__'

class UniversitySerializer(serializers.ModelSerializer):
    class Meta:
        model = University
        fields = '__all__'

class InstituteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institute
        fields = '__all__'

class ProjectPolicySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectPolicy
        fields = '__all__'
        depth = 1

class Policy_kindSerializer(serializers.ModelSerializer):
    class Meta:
        model = Policy_kind
        fields = '__all__'

class ChecklistTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChecklistTask
        fields = '__all__'

class ProjectChecklistTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectChecklistTask
        fields = '__all__'