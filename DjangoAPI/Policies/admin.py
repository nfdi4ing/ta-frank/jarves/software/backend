from django.contrib import admin
from .models import Policyguide, Policy_kind, DMP_Tool, Storage_Tool, Funding, Institute, ProjectPolicy, ChecklistTask, ProjectChecklistTask
from .models import Colleg as University
# Register your models here.

admin.site.register(Policyguide)
admin.site.register(Policy_kind)
admin.site.register(DMP_Tool)
admin.site.register(Storage_Tool)
admin.site.register(Funding)
admin.site.register(University)
admin.site.register(Institute)
admin.site.register(ProjectPolicy)
admin.site.register(ChecklistTask)
admin.site.register(ProjectChecklistTask)