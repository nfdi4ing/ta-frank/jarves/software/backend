# Generated by Django 4.0.3 on 2024-04-24 11:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Policies', '0011_remove_projectpolicy_policy_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='funding',
            old_name='url',
            new_name='acronym',
        ),
        migrations.AddField(
            model_name='funding',
            name='policy',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='Policies.policyguide'),
        ),
    ]
