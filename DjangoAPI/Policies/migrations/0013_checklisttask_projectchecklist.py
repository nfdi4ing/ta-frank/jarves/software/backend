# Generated by Django 4.0.3 on 2024-04-30 13:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ProjectAPP', '0005_remove_employee_institute_and_more'),
        ('Policies', '0012_rename_url_funding_acronym_funding_policy'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChecklistTask',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='NONE', max_length=30)),
                ('description_en', models.CharField(default='NONE', max_length=300)),
                ('description_de', models.CharField(default='NONE', max_length=300)),
                ('tag', models.CharField(max_length=30)),
                ('done', models.BooleanField()),
                ('phase', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ProjectAPP.projectphase')),
                ('step', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ProjectAPP.projectstep')),
            ],
        ),
        migrations.CreateModel(
            name='ProjectChecklist',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='NONE', max_length=30)),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ProjectAPP.project')),
                ('tasks', models.ManyToManyField(to='Policies.checklisttask')),
            ],
        ),
    ]
