from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

# ViewSets are registered with a router class
router.register(r'ChecklistTaskViewSet', views.ChecklistTaskViewSet, basename='ChecklistTaskViewSet')

urlpatterns = [
    path('', include(router.urls)),
    path('ChecklistTaskView/<int:pk>', views.SingleChecklistTaskView.as_view(), name='checklist_task-detail'),
    path('dmp_tools/', views.dmp_tool_list, name='dmp_tool_list'),
    path('dmp_tools/<int:pk>/', views.dmp_tool_detail, name='dmp_tool_detail'),
    path('dmp_tools/create/', views.create_dmp_tool, name='create_dmp_tool'),
    path('storage-tools/', views.storage_tool_list, name='storage_tool_list'),
    path('storage-tools/<int:pk>/', views.storage_tool_detail, name='storage_tool_detail'),
    path('storage-tools/create/', views.create_storage_tool, name='create_storage_tool'),
    path('funding/', views.funding_list, name='funding_list'),
    path('funding/<int:pk>/', views.funding_detail, name='funding_detail'),
    path('funding/create/', views.create_funding, name='create_funding'),
    path('policies/', views.policy_list, name='policy_list'),
    path('policies/<int:pk>/', views.policy_detail, name='policy_detail'),
    path('policies/create/', views.create_policy, name='create_policy'),
    path('universities/', views.university_list, name='university_list'),
    path('universities/<int:pk>/', views.university_detail, name='university_detail'),
    path('universities/create/', views.create_university, name='create_university'),
    path('institutes/', views.institute_list, name='institute_list'),
    path('institutes/<int:pk>/', views.institute_detail, name='institute_detail'),
    path('institutes/create/', views.create_institute, name='create_institute'),
    path('project-policies/<int:pid>/', views.projectpolicy_list, name='projectpolicy_list'),
    path('project-policies/<int:pk>/', views.projectpolicy_detail, name='projectpolicy_detail'),
    path('project-policies/create/', views.create_projectpolicy, name='create_projectpolicy'),
    path('policy-kinds/', views.policy_kind_list, name='policy_kind_list'),
    path('policy-kinds/<int:pk>/', views.policy_kind_detail, name='policy_kind_detail'),
    path('policy-kinds/create/', views.create_policy_kind, name='create_policy_kind'),
    path('checklist_task/create/', views.create_checklist_task, name='create_checklist_task'),
    path('project_checklist/create/', views.create_project_checklist, name='create_project_checklist'),
    path('dummy-upload/', views.dummy_file_upload, name='dummy-upload'),
    path('dummy-download/', views.dummy_file_download, name='dummy-download'),
]
