from django.db import models

# Create your models here.

class DMP_Tool(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default='NONE')
    url = models.CharField(max_length=30, default='NONE')

class Storage_Tool(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default='NONE')
    url = models.CharField(max_length=30, default='NONE')

class ProjectPolicy(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=100, default='NONE')
    kind  = models.ForeignKey(
        "Policy_kind", 
        on_delete=models.CASCADE)
    project = models.ForeignKey('ProjectAPP.Project', on_delete=models.CASCADE)
    dmp_recommendation = models.ForeignKey(DMP_Tool, on_delete=models.CASCADE, null=True)
    storage_recommendation = models.ForeignKey(Storage_Tool, on_delete=models.CASCADE, null=True)
    score1 = models.CharField(max_length=30, default='NONE')
    score2 = models.CharField(max_length=30, default='NONE')

class Policy_kind(models.Model):
    def __str__(self) -> str:
        return str(self.name)
    name = models.CharField(max_length=30)

class ChecklistTask(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default='NONE')
    description_en = models.CharField(max_length=300, default='NONE')
    description_de = models.CharField(max_length=300, default='NONE')
    responsible = models.CharField(max_length=30, default='NONE')
    done = models.BooleanField()
    phase = models.ForeignKey('ProjectAPP.ProjectPhase', on_delete=models.CASCADE)
    step = models.ForeignKey('ProjectAPP.ProjectStep', on_delete=models.CASCADE)
    tag = models.CharField(max_length=30)

class ProjectChecklistTask(models.Model):
    def __str__(self) -> str:
        return self.name
    project = models.ForeignKey('ProjectAPP.Project', on_delete=models.CASCADE)
    name = models.CharField(max_length=30, default='Todo: ')
    description_en = models.CharField(max_length=300, default='NONE')
    description_de = models.CharField(max_length=300, default='NONE')
    responsible = models.CharField(max_length=30, default='NONE')
    done = models.BooleanField(default=False)
    phase = models.ForeignKey('ProjectAPP.ProjectPhase', on_delete=models.CASCADE)
    step = models.ForeignKey('ProjectAPP.ProjectStep', on_delete=models.CASCADE)
    tag = models.CharField(max_length=30)

#university and policy

class Policyguide(models.Model):
    def __str__(self) -> str:
        return str(self.name)
    name = models.CharField(max_length=100)
    kind  = models.ForeignKey(
        "Policy_kind", 
        on_delete=models.CASCADE)
    tasks = models.ManyToManyField(ChecklistTask)
    dmp_recommendation = models.ForeignKey(DMP_Tool, on_delete=models.CASCADE, null=True)
    storage_recommendation = models.ForeignKey(Storage_Tool, on_delete=models.CASCADE, null=True)
    score1 = models.CharField(max_length=30, default='NONE')
    score2 = models.CharField(max_length=30, default='NONE')

class Colleg(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=30, default='NONE')
    policy = models.OneToOneField(Policyguide, on_delete=models.CASCADE, null=True)
    acronym = models.CharField(max_length=30)

class Institute(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=100, default='NONE')
    policy = models.OneToOneField(Policyguide, on_delete=models.CASCADE, null=True)
    university = models.ForeignKey(Colleg, on_delete=models.CASCADE, null=True)
    acronym = models.CharField(max_length=30)

class Funding(models.Model):
    def __str__(self) -> str:
        return self.name
    name = models.CharField(max_length=100, default='NONE')
    acronym = models.CharField(max_length=30, default='NONE')
    policy = models.ForeignKey(Policyguide, on_delete=models.CASCADE, null=True)