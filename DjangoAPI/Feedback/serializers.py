
from rest_framework import serializers

from .models import NeedForHelp


class NeedForHelpSerializer(serializers.HyperlinkedModelSerializer):
    # url= serializers.HyperlinkedIdentityField(view_name='ProjectViewSet')

    class Meta:
        model = NeedForHelp
        fields = ['datetime','number']

