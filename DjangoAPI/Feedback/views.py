# from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import RetrieveAPIView
from .serializers import NeedForHelpSerializer
from .models import NeedForHelp
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

import logging

# Initialize logger
logger = logging.getLogger(__name__)

# no @login_required 
""" def NeedForHelpView(request)->HttpResponse:
    return HttpResponse("Hello world") """

""" class LatestNeedForHelpView (RetrieveAPIView):
    queryset = NeedForHelp.objects.latest("datetime")
    serializer_class = NeedForHelpSerializer """



class NeedForHelpViewSet(ModelViewSet):
    
    # Queryset of all the objects availiable in the view.
    # queryset = Project.objects.all()
    # projectcount = queryset.count()
    NeedForHelpcount = None  # TODO: is this ever read before writing?

    # Select Serializer to serialize, validate, deserialize input and output
    serializer_class = NeedForHelpSerializer

    # Logging actions
    logger.debug("NeedForHelpViewSet has been executed")

    def create(self,request):

        try :
            LatestNeedForHelp = NeedForHelp.objects.latest('datetime')
            LatestNumber=LatestNeedForHelp.number
        except:
            LatestNumber=0

        record=NeedForHelp(number=LatestNumber+1)
        record.save()

        response_data= {"Message":"Created Sucessfully"}
        return JsonResponse(response_data, status=201)


    def get_queryset(self)->NeedForHelp:
        """
        Optionally restricts the returned projects to a given title,
        by filtering against a `title` query parameter in the URL.
        """

        # Get the search criteria for the queryset
        #title = self.request.query_params.get('title', None)
        attribute = self.request.query_params.get('attribute', None)
        filter = self.request.query_params.get('filter', None)
        page_number = self.request.query_params.get('pageNumber', None)
        page_size = self.request.query_params.get('pageSize', None) 
        sorted_by = self.request.query_params.get('sortedBy', None)
        sort_order = self.request.query_params.get('sortOrder', None)

        Latest = self.request.query_params.get('Latest', None)
        
        if Latest=="True":
            try :
                LatestNeedForHelp = NeedForHelp.objects.latest('number')
                queryset = NeedForHelp.objects.filter(pk=LatestNeedForHelp.pk)
                print(LatestNeedForHelp)
            except:
                queryset = NeedForHelp.objects.all()
            
            print (queryset)
        else:
            queryset = NeedForHelp.objects.all()
            print (queryset)
            
        return queryset

          
        """ # Get the queryset:
        # the order_by () only works while creating the queryset, so the workaround is to first check what order is wanted and then create the queryset, 
        # watch out for the hardcoded conditions of sort_order (asc, desc) but it works...
        if sorted_by is None:
            queryset = NeedForHelp.objects.all().order_by('id')

        if sorted_by is not None:
            if sort_order == 'asc' or sort_order is None :
                 queryset = NeedForHelp.objects.all().order_by(sorted_by)

            if sort_order == 'desc' :
                 queryset = NeedForHelp.objects.all().order_by('-' + sorted_by)

        '''
        # If there is search criteria for the queryset, filter queryset by it, otherwise leave returned data untouched.
        if title is not None:
            queryset = queryset.filter(title__icontains = title)
            # __icontains means, that the title param may be included in the results, while the i of icontains means it is NOT case sensitive.
        '''

        if filter is not None:
            if attribute is not None:
                if not hasattr(NeedForHelp,attribute): # Django sql injection, risk?
                    raise ValueError(f'given {attribute=} is not applicable')
                
                kwargs = {f'{attribute}__icontains': filter}
                # sets kwargs dynamicly, queryset.filter(f'{attribute}__icontains'= filter), puts the string in attribute in the **__icontains command and checks for filter
                queryset = queryset.filter(**kwargs)
                # __icontains means, that the title param may be included in the results, while the i of icontains means it is NOT case sensitive.
                self.projectcount = queryset.count()
        else:
            self.projectcount = queryset.count()


        if page_size is not None:
            # paginator, the paginator sequences the items in pages of item amount 'page_size'
            # it only does so if the page_size is not None
            paginator = Paginator(queryset, per_page = page_size)

            # Must be raised by one because first page in angular is 0 and django first page is 1 and so on
            page_number= 1 + int(page_number)
            
            #if page_number == 0:
            #    page_number = 1
            page_obj = paginator.get_page(page_number)
            
            # If the page isn’t a number, it returns the first page. If the page number is negative or greater than the number of pages, it returns the last page (https://docs.djangoproject.com/en/4.1/ref/paginator/#django.core.paginator.Paginator.page)
            # apparently also applies for page_number = 0

            return page_obj 


        return queryset"""
