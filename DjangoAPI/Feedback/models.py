# from django.db import models
from django.db import models
import datetime


# For info about Field types, see: https://docs.djangoproject.com/en/4.0/ref/models/fields/#field-types
# For info about custom fields, see: https://docs.djangoproject.com/en/4.0/howto/custom-model-fields/
# For info about field relationships, see: https://docs.djangoproject.com/en/4.0/topics/db/models/#relationships

# Create your models here.

# This is a button on the Landing page, where people can state to need help
class NeedForHelp(models.Model):
    def __str__(self) -> str:
        return self.datetime.strftime('%m/%d/%Y %H:%M:%S')
    
    datetime = models.DateTimeField(auto_now_add=True)
    number= models.IntegerField(default=1)

"""
# Beispiel PLM TU Berlin

class Teilestammsatz(models.Model):
        TeileNr = models.CharField(max_length=None)
        Revision = models.CharField(max_length=None)
        Benennung = models.CharField(max_length=None)


class Dokumentenstammsatz(models.Model):
        DokNr = models.CharField(max_length=None)
        Revision = models.CharField(max_length=None)
        Benennung = models.CharField(max_length=None)
        Typ = models.CharField(max_length=None)

        # 1:m Beziehung:
        # Ein Dokumentensammsatz kann viele Dateien beinhalten.
        date = models.ForeignKey(Datei, on_delete=models.CASCADE)

        # n:m Beziehung:
        # Ein Dokumentensammsatz kann viele Teilestammsätze
        # beinhalten und umgekehrt.
        teilestammsaetze= models.ManyToManyField(Teilestammsatz)

class Datei(models.Model):
        # Upload field, um Dateien zu speichern.
        upload = models.FileField(upload_to='uploads/')
"""
