from django.urls import path, include
from django.db import router
from rest_framework.routers import DefaultRouter
from . import views


router = DefaultRouter()


router.register(r'NeedForHelpViewSet', views.NeedForHelpViewSet, basename='NeedForHelpViewSet')

urlpatterns = [
    path('', include(router.urls)), 
    # path('NeedForHelp', views.NeedForHelpView, name='NeedForHelp'),
    #path('index2/', views.index2),
]
