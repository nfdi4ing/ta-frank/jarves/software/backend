from django.db import router
from django.urls import path, include
# from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()


# ViewSets are registered with a router class
router.register(r'TrainingViewSet', views.TrainingViewSet, basename='TrainingViewSet')

urlpatterns = [
    path('', include(router.urls)),
    path('TrainingView/<int:pk>', views.SingleTrainingView.as_view(), name='training-detail'),
]
