
from rest_framework import serializers

from .models import Training


class TrainingSerializer(serializers.HyperlinkedModelSerializer):
    # url= serializers.HyperlinkedIdentityField(view_name='projectphase-detail')

    class Meta:
        model = Training
        fields = ['url', 'id', 'title', 'link', 'language', 'phase', 'step', 'tag', 'author', 'format', 'comment', 'rating']
