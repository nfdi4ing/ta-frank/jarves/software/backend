from django.shortcuts import render
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.viewsets import ModelViewSet
from django.core.paginator import Paginator
from django.db.models import Q

import logging

from .models import Training
from .serializers import TrainingSerializer
from ProjectAPP.models import ProjectPhase, ProjectStep
from ProjectAPP.serializers import ProjectPhaseSerializer

# Initialize logger
logger = logging.getLogger(__name__)

# Create your views here.

class SingleTrainingView (RetrieveUpdateDestroyAPIView):
    queryset = Training.objects.all()
    serializer_class = TrainingSerializer


class TrainingViewSet(ModelViewSet):

    # Queryset of all the objects availiable in the view.
    queryset = Training.objects.all()
    TrainingCount = queryset.count()

    # Select Serializer to serialize, validate, deserialize input and output
    serializer_class = TrainingSerializer

    # Logging actions
    logger.debug("TrainingViewSet executes")

    
    def get_queryset(self)->Training:
        """
        Optionally restricts the returned trainings to given params
        """


        # Extract Values from Request
        self.extract_params_from_request()

        # Get all
        queryset = Training.objects.all()

        # Get correct queryset
        queryset = self.filter_queryset(queryset)

        # Sort Queryset
        # TODO: Sorting goes here
        
        # Paginate Querryset
        queryset = self.paginate_response(queryset)


        # TODO: Write Paginator and sorting Function and import.

        

        return queryset
    
    def filter_queryset(self,queryset):
        # Here the logic for the returned query is inserted

        # TODO: LOGIC GOES HERE

        # Filtering of querysets:
        # https://www.w3schools.com/django/django_queryset_filter.php 

        # Filter for Phase

        if self.phase is not None or self.step is not None:

            # Get All_Phase and All_Step
            All_Phase = ProjectPhase.objects.get(CODE_name = 'All')
            All_Step = ProjectStep.objects.get(CODE_name = 'All')
            
            # Get ID of the requested Phase from the Phases 
            try:
                Phase = ProjectPhase.objects.get(CODE_name = self.phase).id
            except: 
                Phase = None

            # Get ID of the requested Step from the Steps 
            try:
                Step = ProjectStep.objects.get(CODE_name = self.step).id
            except:
                Step = None

            # Get the queryset
            queryset = Training.objects.filter( 
                Q(phase = Phase) | Q(phase = All_Phase),
                Q(step = Step) | Q(step = All_Step)
                ).distinct()
        
        # Get count of results in queryset
        self.TrainingCount = queryset.count()
            

        # Filter for Step



        # Filter for Language
        #if self.language is not None:
            #queryset = queryset.objects.filter(language = self.language)

        return queryset
            

    def paginate_response(self,queryset):
        if self.page_size is not None:
            # paginator, the paginator sequences the items in pages of item amount 'page_size'
            # it only does so if the page_size is not None
            paginator = Paginator(queryset, per_page = self.page_size)

            # Must be raised by one because first page in angular is 0 and django first page is 1 and so on

            if self.page_number is not None:
                page_number= 1 + int(self.page_number)
            else:
                page_number=1
            
            #if page_number == 0:
            #    page_number = 1
            page_obj = paginator.get_page(page_number)
            
            # If the page isn’t a number, it returns the first page. If the page number is negative or greater than the number of pages, it returns the last page (https://docs.djangoproject.com/en/4.1/ref/paginator/#django.core.paginator.Paginator.page)
            # apparently also applies for page_number = 0

            return page_obj
        return queryset
    
    def extract_params_from_request (self):
        # Extract Values from Request
        self.title = self.request.query_params.get('title', None)
        self.project = self.request.query_params.get('project', None)
        self.fit = self.request.query_params.get('fit', None)
        self.sortOrder = self.request.query_params.get('sortOrder', None)
        self.sortAttribute = self.request.query_params.get('sortAttribute', None)
        self.pageIndex = self.request.query_params.get('pageIndex', None)
        self.pageSize = self.request.query_params.get('pageSize', None)
        self.language = self.request.query_params.get('language', None)
        self.phase = self.request.query_params.get('phase', None)
        self.step = self.request.query_params.get('step', None)
        self.tag = self.request.query_params.get('tag', None)
        self.author = self.request.query_params.get('author', None)
        self.format = self.request.query_params.get('format', None)
        self.comment = self.request.query_params.get('comment', None)
        self.rating = self.request.query_params.get('rating', None)
        self.attribute = self.request.query_params.get('attribute', None)
        self.filter = self.request.query_params.get('filter', None)
        self.page_number = self.request.query_params.get('pageNumber', None)
        self.page_size = self.request.query_params.get('pageSize', None) 
        self.sorted_by = self.request.query_params.get('sortedBy', None)
        self.sort_order = self.request.query_params.get('sortOrder', None)
      
    def finalize_response(self, request, response, *args, **kwargs):
        response = super().finalize_response(request, response, *args, **kwargs)

        # Write TrainingCount into header
        response['TrainingCount'] = self.TrainingCount

        # Enables 'TrainingCount' to be read from header
        response['Access-Control-Expose-Headers'] = 'TrainingCount'
        return response
    
    
