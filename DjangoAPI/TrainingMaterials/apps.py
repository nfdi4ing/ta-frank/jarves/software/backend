from django.apps import AppConfig


class TrainingmaterialsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'TrainingMaterials'
