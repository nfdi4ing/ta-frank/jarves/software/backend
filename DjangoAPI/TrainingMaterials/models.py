from django.db import models
from ProjectAPP.models import ProjectPhase, ProjectStep

# Create your models here.


class Training(models.Model):
    def __str__(self):
        return self.title
    
    # id
    id = models.AutoField(primary_key=True)
    
    # Titel der Unterlagen
    title = models.CharField(max_length=100)
    
    # Link zu den Unterlagen
    link = models.URLField(max_length=1000)

    # Sprache der Materialien
    language = models.CharField(max_length=30)

    # Phase der Unterlagen
    phase = models.ManyToManyField(ProjectPhase)
    
    # Schritt der Unterlagen
    step = models.ManyToManyField(ProjectStep)
    # step = models.CharField(max_length=30, default='-')
    
    # Tags der Unterlagen
    tag = models.CharField(max_length=30)
    
    # Autor(en) der Materialien
    author = models.CharField(max_length=100)
    
    # Format (Foliensatz, Interaktiv, Video etc.)
    format = models.CharField(max_length=100)
    
    # Commentarfeld just in case
    comment = models.CharField(max_length=100)
    
    # Bewertung der Materialien --> Soll im Frontend dann + oder - gewertet werden und hier den wert +1 oder -1 rechnen
    rating = models.BigIntegerField(
        default=0
    )
    