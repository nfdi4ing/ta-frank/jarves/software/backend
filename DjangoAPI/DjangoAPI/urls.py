
# Description: Contains URL declarations for the Django project.

"""DjangoAPI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework_swagger.views import get_swagger_view
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf.urls import include
from django.urls import re_path
from django.conf import settings
from django.views.generic import RedirectView
# The following defines all URLs provided by the backend.
#
# The first argument defines a URL link as expasion to the link.
# For example `admin/` gets added to `http://127.0.0.1:8000/` (considering you are on your localhost), which adds up to: http://127.0.0.1:8000/admin/
# Links may bee choosen as you like but in development i reccomend to use the same URL expansion as the path you are directing to for readability reasons.
#
# The second argument redirects the request to the correct place within the Backend, which is either a new redirect or a view/API-View.

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path('admin/', admin.site.urls),  # http://127.0.0.1:8000/admin/
    path('Feedback/', include('Feedback.urls')),
    path('ProjectAPP/', include('ProjectAPP.urls')),
    path('Policies/', include('Policies.urls')),
    path('apiservices/', include('apiservices.urls')),
    path('accounts/', include('allauth.urls')),
    path('trainings/', include('TrainingMaterials.urls')),
    re_path(r'^accounts/profile', RedirectView.as_view(url=f'{settings.URL_ROOT_FRONTEND}/projects', permanent=False), name='index'),
    re_path(r'^accounts/orcid/login/callback', RedirectView.as_view(url=f'{settings.URL_ROOT_FRONTEND}/projects', permanent=False), name='index'),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
