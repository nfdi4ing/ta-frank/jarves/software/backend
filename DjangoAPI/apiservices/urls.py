from django.urls import path
from . import views

app_name = 'apiservices'

urlpatterns = [
    path('get-logged-in-user/', views.get_logged_in_user, name='get_logged_in_user'),
    path('get-all-users/', views.get_all_users, name='get_all_users'),
    path('email/', views.UserProfileViewSet.as_view({'get': 'get_email', 'post': 'update_email'}), name='email_account'),
    path('api-key/', views.UserProfileViewSet.as_view({'get': 'get_api_key', 'post': 'update_api_key'}), name='api_key'),
    path('user-id/', views.UserProfileViewSet.as_view({'get': 'get_user_id'}), name='user_id'),
    path('execute-request/<str:param1>/<str:param2>/', views.create_project_rdmo, name='execute_request'),
    path('api-key-coscine/', views.UserProfileViewSet.as_view({'get': 'get_api_key_coscine','post': 'update_api_key_coscine'}), name='update_api_key_coscine'),
    path('execute-request-coscine/<str:param1>/<str:param2>/', views.create_project_coscine, name='create_project_coscine'),
    path('darkmode/', views.UserProfileViewSet.as_view({'get': 'get_dark_mode', 'post': 'update_dark_mode'}), name='dark_mode'),
    path('users/', views.UserProfileViewSet.as_view({'get': 'get_usernames'}), name='user_name'),
]