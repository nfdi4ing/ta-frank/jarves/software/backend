from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    api_key = models.CharField(max_length=100, default='post a rdmo api key here')
    api_key_coscine = models.CharField(max_length=300, default='post a coscine api key here')
    settings_darkmode = models.BooleanField(default=True)
    email_account = models.CharField(max_length=300, default='save your email here')

    def __str__(self) -> str:
        return str(self.user)

# Signal receiver function to create UserProfile when a new User is created
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

# Connect the signal
post_save.connect(create_user_profile, sender=User)
