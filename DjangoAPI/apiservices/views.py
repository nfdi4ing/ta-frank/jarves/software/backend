from django.shortcuts import render
import requests
import json
from django.shortcuts import render
from .forms import UserProfileForm
from .models import UserProfile
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets

import coscine
from datetime import datetime

from rest_framework.decorators import action

from rest_framework import serializers
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from rdmo_client import Client

@login_required
def get_logged_in_user(request):
    user = request.user
    # Access user properties
    username = user.username
    email = user.email
    # ... other user properties

    # Return user information as a JSON response
    response_data = {
        'username': username,
        'email': email,
        #'id' : id,
        # ... other user properties
    }
    return JsonResponse(response_data)

@login_required
def get_all_users(request):
    # Get all user profiles
    user_profiles = UserProfile.objects.all()

    # Extract user information from user profiles
    users_info = []
    for profile in user_profiles:
        user_info = {
            'username': profile.user.username,
            'first_name': profile.user.first_name,
            'last_name': profile.user.last_name,
            'email': profile.user.email,
            'id': profile.user.id,
            # Add other user properties if needed
        }
        users_info.append(profile.user.id)

    # Return user information as a JSON response
    response_data = {
        'users': users_info,
    }
    return JsonResponse(response_data)

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ['api_key', 'api_key_coscine', 'email_account', "user"]

class UserProfileViewSet(viewsets.ModelViewSet):
    allowed_methods = ['GET', 'POST']
    serializer_class = UserProfileSerializer
    # Specify the authentication classes
    authentication_classes = [SessionAuthentication]
    # Specify the permission classes
    permission_classes = [IsAuthenticated]

    @action(detail=False, methods=['get'])
    def get_email(self, request):
        user_profile = UserProfile.objects.filter(user=request.user).first()
        if user_profile:
            email_account = user_profile.email_account 
            return JsonResponse({'key': email_account})
        else:
            return JsonResponse({'error': 'Email not found'}, status=400)

    @action(detail=False, methods=['post'])
    def update_email(self, request):
        email_account = request.data.get('key')
        user_profile, created = UserProfile.objects.get_or_create(user=request.user)
        print(email_account)

        try:
            user_profile.email_account = str(email_account)
            user_profile.save()
            return JsonResponse({'key': email_account})
        except UserProfile.DoesNotExist:
            print("Email not found for the currrent user")
            return JsonResponse({'error': 'Email not found'}, status=400)

    @action(detail=False, methods=['get'])
    def get_usernames(self, request):
        user_profile = User.objects.all().values()
        username = {}
        """ if user_profile:
            for userdata in user_profile:
                username[userdata.get_full_name()] = userdata.id
            return JsonResponse(username, safe= False) """
        if user_profile:
            #return JsonResponse({'usernames': list(user_profile)})
            return JsonResponse(list(user_profile), safe= False)
        else:
            return JsonResponse({'error': 'Username not found'}, status=400)
        
    @action(detail=False, methods=['get'])
    def get_user_id(self, request):
        user_profile = UserProfile.objects.filter(user=request.user).first()
        if user_profile:
            user_id = user_profile.user_id
            return JsonResponse({'key': user_id})
        else:
            return JsonResponse({'error': 'user id not found'}, status=400)

    @action(detail=False, methods=['get'])
    def get_api_key(self, request):
        user_profile = UserProfile.objects.filter(user=request.user).first()
        if user_profile:
            api_key = user_profile.api_key
            return JsonResponse({'key': api_key})
        else:
            return JsonResponse({'error': 'API key not found'}, status=400)

    @action(detail=False, methods=['post'])
    def update_api_key(self, request):
        key = request.data.get('key')
        user_profile, created = UserProfile.objects.get_or_create(user=request.user)

        try:
            user_profile.api_key = str(key)
            user_profile.save()
        except UserProfile.DoesNotExist:
            print("API key not found for the current user.")
            return JsonResponse({'error': 'API key not found'}, status=400)

        return JsonResponse({'message': 'Key updated successfully'})

    @action(detail=False, methods=['get'])
    def get_api_key_coscine(self, request):
        user_profile = UserProfile.objects.filter(user=request.user).first()
        if user_profile:
            api_key_coscine = user_profile.api_key_coscine
            return JsonResponse({'key': api_key_coscine})
        else:
            return JsonResponse({'error': 'API key not found'}, status=400)

    @action(detail=False, methods=['post'])
    def update_api_key_coscine(self, request):
        user_profile, created = UserProfile.objects.get_or_create(user=request.user)
        api_key_coscine = request.data.get('key')

        #if api_key_coscine is None:
        #    return JsonResponse({'error': 'No key provided'}, status=400)

        try:
            user_profile.api_key_coscine = str(api_key_coscine)
            user_profile.save()
        except UserProfile.DoesNotExist:
            print("API key not found for the current user.")
            return JsonResponse({'error': 'API key not found'}, status=400)

        return JsonResponse({'message': 'Key updated succesfully'})

    @action(detail=False, methods=['get'])
    def get_dark_mode(self, request):
        user_profile = UserProfile.objects.filter(user=request.user).first()
        if user_profile:
            value = user_profile.settings_darkmode
            return JsonResponse({'darkmode': value})
        else:
            return JsonResponse({'error': 'User not found'}, status=400)

    @action(detail=False, methods=['post'])
    def update_dark_mode(self, request):
        user_profile, created = UserProfile.objects.get_or_create(user=request.user)
        new_value = request.data.get('darkmode')

        if new_value is None:
            return JsonResponse({'error': 'Invalid setting'}, status=400)

        try:
            user_profile.settings_darkmode = new_value
            user_profile.save()
        except UserProfile.DoesNotExist:
            print("Don't think this is possible")
            return JsonResponse({'error': 'Setting not found'}, status=400)

        return JsonResponse({'message': 'Setting updated successfully'})



def create_project_rdmo(request, param1, param2):

    # Specify the authentication classes
    authentication_classes = [SessionAuthentication]
    
    # Specify the permission classes
    permission_classes = [IsAuthenticated]

    try:
        user_profile = UserProfile.objects.get(user=request.user)
        api_key = user_profile.api_key
    except UserProfile.DoesNotExist:
        print("API key not found for the current user.")
        return JsonResponse({'error': 'API key not found'}, status=400)

    if not api_key:
        print("API key is not set for the current user.")
        return JsonResponse({'error': 'API key not set'}, status=400)

    project_title = param1
    description = param2
    url = 'https://rdmo-test.nfdi4ing.ulb.tu-darmstadt.de/'

    client = Client(url, auth=None,token=str(api_key))

    try: 
        client.create_project({
            "title": str(project_title),
            "description": str(description),
        })
        return JsonResponse({'message': 'Project created succssfully'})
    except:
        return JsonResponse({'message': 'Project could not be created'})


def create_project_coscine(request, param1, param2):


    # Specify the authentication classes
    authentication_classes = [SessionAuthentication]
    
    # Specify the permission classes
    permission_classes = [IsAuthenticated]

    try:
        user_profile = UserProfile.objects.get(user=request.user)
        api_key_coscine = user_profile.api_key_coscine
        
    except UserProfile.DoesNotExist:
        print("API key not found for the current user.")
        return JsonResponse({'error': 'API key not found'}, status=400)

    if not api_key_coscine:
        print("API key is not set for the current user.")
        return JsonResponse({'error': 'API key not set'}, status=400)

    project_title = param1
    description = param2
    token = api_key_coscine 

    client = coscine.Client(token)
    print(token)
    form = client.project_form()

    data: dict = {
            "Project Name": project_title,
            "Display Name": project_title,
            "Project Description": description,
            "Principal Investigators": "Test Principal Investigator",
            "Project Start": datetime.now(),
            "Project End": datetime.now(),
            "Discipline": ["Computer Science 409"],
            "Participating Organizations": ["https://ror.org/04xfq0f34"],
            "Project Keywords": "Test Keywords",
            "Metadata Visibility": "Project Members",
            #"Grant ID": "Test Grant ID"
            }
    print(data)
    try:
        form = client.project_form(data)
        client.create_project(form)
        
        return JsonResponse({'message': 'Project created successfully'})
    except:
        return JsonResponse({'message': 'Failed to create project'})

    #url = "https://coscine.rwth-aachen.de/coscine/api/Coscine.Api.Project/Project"

    #headers = {
    #    "Content-Type": "application/json",
    #    "Authorization": "Bearer " + str(api_key_coscine), 
    #    "Content-Type": "application/json"
    #}

    #data = {
    #"id": "random",
    #"pid": "random",
    #"description": "random",
    #"displayName": "string",
    #"startDate": "2023-08-05T06:21:17.807Z",
    #"endDate": "2023-08-05T06:21:17.807Z",
    #"keywords": description,
    #"projectName": project_title,
    #"principleInvestigators": "random",
    #"grantId": "string",
    #"slug": "random",
    #"dateCreated": "2023-07-05T06:21:17.807Z",
    #"disciplines": [
    #    {
    #        "id": "string",
    #        "url": "string",
    #        "displayNameDe": "string",
    #        "displayNameEn": "string"
    #    }
    #],
    #"organizations": [
    #    {
    #        "url": "string",
    #        "displayName": "string"
    #    }
    #],
    #"visibility": {
    #    "id": "string",
    #    "displayName": "string"
    #},
    #"parentId": "string",
    #"creator": "string",
    #"deleted": True
    #}

    #response = requests.post(url, headers=headers, data=json.dumps(data))
    #print(response)
    #if response.status_code == 201:
    #    created_project = response.json()
    #    return JsonResponse({'message': 'Project created successfully'})
    #else:
    #    return JsonResponse({'message': 'Failed to create project'})


   