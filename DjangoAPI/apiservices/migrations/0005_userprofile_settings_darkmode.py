# Generated by Django 4.0.3 on 2023-07-15 15:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apiservices', '0004_alter_userprofile_api_key_coscine'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='settings_darkmode',
            field=models.BooleanField(default=False),
        ),
    ]
